// Armamos las Rutas para  nuestro cliente
const express = require('express');
const router = express.Router();
const { crearEmpleado, mostrarEmpleados, actualizarEmpleado, eliminarEmpleado } = require('../controllers/empleadoController');


// rutas CRUD

router.route('/').get(mostrarEmpleados).post(crearEmpleado)
router.route('/:id').put(actualizarEmpleado).delete(eliminarEmpleado)


module.exports = router;