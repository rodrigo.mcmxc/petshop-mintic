// Armamos las Rutas para  nuestro cliente
const express = require('express');
const router = express.Router();
const { crearCliente, mostrarClientes, actualizarCliente, eliminarCliente } = require('../controllers/clienteController');


// rutas CRUD

router.route('/').get(mostrarClientes).post(crearCliente)
router.route('/:id').put(actualizarCliente).delete(eliminarCliente)


module.exports = router;

