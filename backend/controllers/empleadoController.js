const Empleado = require("../models/Empleado");

exports.crearEmpleado = async (req, res) => {

    try {
        let empleado;
        // creamos nuestro empleado
        empleado = new Empleado(req.body);
        await empleado.save();
        res.send(empleado);

    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
}

exports.mostrarEmpleados = async (req, res) => {

    try {
        const empleado = await Empleado.find();
        res.json(empleado)

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");
    }
}
exports.actualizarEmpleado = async (req, res) => {
    try {
        const { nombre, cargo, email, telefono } = req.body;
        let empleado = await Empleado.findById(req.params.id);
        if (!empleado) {
            res.status(404).json({ msg: 'el empleado no existe' })
        }
        empleado.nombre = nombre;
        empleado.cargo = cargo;
        empleado.email = email;
        empleado.telefono = telefono;

        empleado = await Empleado.findOneAndUpdate({ _id: req.params.id }, empleado, { new: true })
        res.json(empleado);

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");
    }
}
exports.eliminarEmpleado = async (req, res) => {
    try {
        let empleado = await Empleado.findById(req.params.id);
        if (!empleado) {
            res.status(404).json({ msg: 'el empleado no existe' })
        }
        await Empleado.findByIdAndRemove({ _id: req.params.id })
        res.json({ msg: 'empleado eliminado con exito' });
    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");

    }
}