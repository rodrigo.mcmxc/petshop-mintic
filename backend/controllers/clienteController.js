const Cliente = require("../models/Cliente");

exports.crearCliente = async (req, res) => {

  try {
    let cliente;
    // creamos nuestro cliente
    cliente = new Cliente(req.body);
    await cliente.save();
    res.send(cliente);

  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
}

exports.mostrarClientes = async (req, res) => {

  try {
    const clientes = await Cliente.find();
    res.json(clientes)

  } catch (error) {
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
  }
}
exports.actualizarCliente = async (req, res) => {
  try {
    const { nombre, edad, favoritos, premium } = req.body;
    let cliente = await Cliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: 'el cliente no existe' })
    }
    cliente.nombre = nombre;
    cliente.edad = edad;
    cliente.favoritos = favoritos;
    cliente.premium = premium;

    cliente = await Cliente.findOneAndUpdate({ _id: req.params.id }, cliente, { new: true })
    res.json(cliente);

  } catch (error) {
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
  }
}
exports.eliminarCliente = async (req, res) => {
  try {
    let cliente = await Cliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: 'el cliente no existe' })
    }
    await Cliente.findByIdAndRemove({ _id: req.params.id })
    res.json({ msg: 'cliente eliminado con exito' });
  } catch (error) {
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

  }
}