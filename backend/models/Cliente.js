const mongoose = require('mongoose');

const clienteSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    edad: {
        type: String,
        required: true
    },
    favoritos: {
        type: String,
        required: true
    },
    premium: {
        type: String,
        required: true
    }


});

module.exports = mongoose.model('Cliente', clienteSchema);