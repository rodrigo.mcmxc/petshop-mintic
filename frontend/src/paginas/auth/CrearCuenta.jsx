import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';

const CrearCuenta = () => {
//



  //Creamos el hook state
  const [usuario, setUsuario] = useState({
    nombre: "",
    email: "",
    password: "",
    confirmar: "",
  });

  //Necesitmos Extrar información del elemento usuario  useState creado

  const { nombre, email, password, confirmar } = usuario;

  //Necesitamos  capturar los valores cada que se digite en la intefaz de usuario  en algunas de las cajas
  //Se le agregan 2 atributos a las cajas de texto value, onChange

  //¿Qué es el evento onChange? El evento onChange en React es una de las funciones predeterminadas en React que nos permiten definir una acción a ejecutar cuando una situación ocurre. Este evento se utiliza comúnmente para tratar los datos introducidos por el usuario en un formulario
  //el evento onChange permite identificar cambios de estado de las variables iniciales
  const onChange = (e) => {
    setUsuario({
      ...usuario, //copia del  state  usuario, Con el símbolo de tres puntos (…), este operador nos permite copiar de manera simple una parte o la totalidad de un elemento array o un objeto en JavaScript.
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.getElementById("nombre").focus();
  }, []);

  const crearCuenta = async () => {
    if (password !== confirmar) {
        const msg = "Las contraseñas son diferentes.";
        swal({
            title: 'Error',
            text: msg,
            icon: 'error',
            buttons: {
                confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                }
            }
        });
    } else if (password.length < 6) {
        const msg = "La contraseña deber ser al menos de 6 caracteres.";
        swal({
            title: 'Error',
            text: msg,
            icon: 'error',
            buttons: {
                confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                }
            }
        });

    } else {
        const data = {
            nombre: usuario.nombre,
            email: usuario.email,
            password: usuario.password
        }
        const response = await APIInvoke.invokePOST("/api/usuario", data);
        const mensaje = response.msg;

        if (mensaje === 'El usuario ya existe') {
            const msg = "El usuario ya existe.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } 
        else 
        {
            const msg = "El usuario fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setUsuario({
                nombre: '',
                email: '',
                password: '',
                confirmar: ''
            })
        }
    }
}
  const onSubmit = (e) => {
    e.preventDefault(); //¿Qué es preventDefault React?  En react si quieres prevenir un comportamiento por defecto o la propagación de un evento debes hacerlo explícitamente llamando los métodos preventDefault()
    crearCuenta();
  };

  return (
    <div className="hold-transition login-page">
      <div className="login-box">
        <div className="login-logo">
          <Link to={"#"}>
            <b>Crear</b> Cuenta
          </Link>
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">Ingrese datos de usuario</p>

            <form onSubmit={onSubmit}>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nombre"
                  id="nombre"
                  name="nombre"
                  value={nombre}
                  onChange={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>

              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={onChange}
                  required
                />

                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>

              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Contraseña"
                  id="password"
                  name="password"
                  value={password}
                  onChange={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>

              <div className="input-group mb-3">
                <input
                  type="password" //Caja insertar Contraseña
                  className="form-control"
                  placeholder="Confirmar Contraseña"
                  id="confirmar"
                  name="confirmar"
                  value={confirmar}
                  onChange={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>

              <div className="social-auth-links text-center mb-3">
                <button type="submit" className="btn btn-block btn-primary">
                  Crear Cuenta
                </button>
                <Link to={"/"} className="btn btn-block btn-danger">
                  Regresar al Login
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CrearCuenta;
